export const GROUP_PREFIX = "group::";
export const TYPE_GROUP = "group";
export const TYPE_TYPE = "type";
export const TYPE_MILESTONE = "milestone";
export const TYPE_ENABLED = "default_enabled";
export const TYPE_HAS_ROLLOUT = "rollout_issue_url";
export const TYPE_HAS_INTRODUCED = "introduced_by_url";
export const TYPE_HAS_FEATURE_ISSUE = "feature_issue_url";

// This is done because the filter bar doesn't like spaces inside strings.
// We replace all the spaces in the group names with this character that looks like a space, but isn't.
export const SEPARATOR = " ";
