const hasFeatureIssue = (flags, filter) =>
  typeof filter === "boolean"
    ? flags.filter(({ feature_issue_url }) => !!feature_issue_url === filter)
    : flags;

export default hasFeatureIssue;
