import { SEPARATOR } from "../constants";

const groupFilter = (flags, groups = []) =>
  groups[0]?.data
    ? groups.reduce(
        (_flags, filter) => [
          ..._flags,
          ...flags.filter(
            ({ group }) => group === filter.data.replaceAll(SEPARATOR, " ")
          ),
        ],
        []
      )
    : flags;

export default groupFilter;
