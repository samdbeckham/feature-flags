const hasRollout = (flags, filter) =>
  typeof filter === "boolean"
    ? flags.filter(({ rollout_issue_url }) => !!rollout_issue_url === filter)
    : flags;

export default hasRollout;
