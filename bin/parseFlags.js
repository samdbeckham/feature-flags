const fs = require("fs");
var glob = require("glob");
const yaml = require("js-yaml");

const FLAGS_DIR = "data/flags/";
const OUTPUT = "data/flags.json";

const isFlagEE = (filename) => filename.match(/\/ee\//);

const getFlagUrl = (filename) => {
  const REPO_URL = "https://gitlab.com/gitlab-org/gitlab/-/blob/master/";
  const REPO_DIR = "config/feature_flags/";
  const FOLDER = `${REPO_URL}${isFlagEE(filename) ? "ee/" : ""}${REPO_DIR}`;

  return `${FOLDER}${filename.replace(FLAGS_DIR, "").replace(/(e|c)e\//, "")}`;
};

const getFlags = new Promise((res, rej) => {
  return glob(`${FLAGS_DIR}**/*.yml`, {}, function (err, filenames) {
    if (err) {
      rej(err);
    }
    let flags = [];
    filenames.forEach((filename) => {
      const flag = yaml.load(fs.readFileSync(filename, "utf-8"));

      if (!flag.milestone) {
        flag.milestone = "unknown";
      }

      // Because sometimes "13.0" is parsed as 13
      if (typeof flag.milestone !== "string") {
        flag.milestone = `${flag.milestone}.0`;
      }

      flag.is_ee = isFlagEE(filename);
      flag.web_url = getFlagUrl(filename);

      flags.push(flag);
    });
    res(flags);
  });
});

getFlags.then((flags) => {
  fs.writeFileSync(OUTPUT, JSON.stringify(flags, null, 2));
});
